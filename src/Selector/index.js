import { arrayOf, bool, func, number, oneOf, shape, string } from 'prop-types';
import React from 'react';

import { SelectorWrapper } from './styles';

const Options = ({ options }) =>
  options.map(({ label, selected, value }) => (
    <option key={value} value={value} selected={selected}>
      {label}
    </option>
  ));

const Selector = ({ value, onChange, options, size }) =>
  options.length === 0 ? null : (
    <SelectorWrapper value={value} onChange={onChange} size={size}>
      <Options options={options} />
    </SelectorWrapper>
  );

Selector.defaultProps = {
  value: null,
  onChange: Function.prototype,
  options: [],
  size: 'regular',
};

Selector.propTypes = {
  value: number,
  onChange: func,
  options: arrayOf(shape({ label: string, selected: bool, value: string })),
  size: oneOf(['small', 'regular', 'big']),
};

export default Selector;
