import styled from 'styled-components';

const fontSizes = {
  small: '0.8em',
  regular: '1em',
  big: '1.5em',
};

export const SelectorWrapper = styled.select(({ size }) => ({
  border: 'none',
  cursor: 'pointer',
  outline: 'none',
  padding: '0.5em',
  fontSize: fontSizes[size],
}));
