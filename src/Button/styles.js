import styled from 'styled-components';

export const sizeStyles = {
  small: {
    fontSize: '0.8em',
  },
  regular: {
    fontSize: '1em',
  },
  big: {
    fontSize: '1.5em',
  },
};

export const StyledButton = styled('button')(({ size }) => ({
  backgroundColor: 'black',
  border: 'none',
  color: 'white',
  cursor: 'pointer',
  outline: 'none',
  padding: '0.5em',
  ...sizeStyles[size],
}));
