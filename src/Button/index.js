import { func, node, oneOf } from 'prop-types';
import React from 'react';

import { sizeStyles, StyledButton } from './styles';

const Button = ({ action, size, children }) => (
  <StyledButton onClick={action} size={size}>
    {children}
  </StyledButton>
);

Button.defaultProps = {
  action: Function.prototype,
  size: 'regular',
};

Button.propTypes = {
  action: func,
  size: oneOf(Object.keys(sizeStyles)),
  children: node.isRequired,
};

export default Button;
